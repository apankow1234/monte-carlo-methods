# Monte Carlo Simulation

## Learning Via Python

* [First Steps](#first-steps)
* [MatPlotLib](#MatPlotLib)
  * [Installing](#Installing)
  * [Tips](#Tips)
  * [Troubleshooting](#Troubleshooting)

### First steps

Understanding simple betting strategies on random chance games

|No Strategy|Martingale Strategy|Martingale vs. No Strategy|
|:---:|:---:|:---:|
| [![Simple Better](screenshots/figure-00-box.png)](screenshots/figure-00-box.png) | [![Simple Better](screenshots/figure-01-box.png)](screenshots/figure-01-box.png) | [![Simple Better](screenshots/figure-02-box.png)](screenshots/figure-02-box.png) |

If the house has even a slight percentage advantage, they will eventually win; always.

### MatPlotLib

#### Installing

If you have Python already installed:
```sh
python -mpip install -U pip
python -mpip install -U matplotlib
```


#### Tips

Auto resize layout so all labels are always visible
```python
from matplotlib import rcParams
# ...
# right before savefig()
rcParams.update({'figure.autolayout': True})
```


#### Troubleshooting

##### ERROR:
```python
MemoryError
```
Try: 
1. Make sure all your code conforms to the version installed with MatPlotLib
  * ~ Python 2.x, `print `
  * ~ Python 3.x, `print()`
2. Refresh the console
  * Windows - Try closing and re-opening `cmd.exe`
  * Linux/ Mac - Try `source filename`
3. Install a full-package build of Python
  * ~ Anaconda, [https://www.anaconda.com/download/](https://www.anaconda.com/download/)


##### ERROR - Windows (7 or newer):
```python
ImportError: DLL load failed: %1 is not a valid Win32 application.
```
Do: 
1. Uninstall all versions of Python on machine
2. Install Python 3.x (your latest version)
  * Install to PATH variable
3. Run `python -mpip install -U matplotlib`
4. Install Python x.x (other versions you have)
5. Done! Try your scripts again.