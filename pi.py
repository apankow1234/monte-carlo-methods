import sys
import numpy as np

def probe_for_unit_circle() :
	# uniformly distributed random numbers for probing a unit square
	x = np.random.rand()
	y = np.random.rand()
	# unit circles inside of unit squares have radii smaller than 1
	return 1 if np.sqrt(x**2+y**2) < 1.0 else 0

if __name__ == "__main__" :
	# Get number of samples, N, from user or default
	N = int(sys.argv[1]) if len(sys.argv) > 1 else 1024

	# Run sampling method N times
	# probing a unit_square for a unit_circle
	results = [probe_for_unit_circle() for n in range(N)] 
	hits = sum(results)

	# Discover meaning from simulation
	print( "pi:", hits/float(len(results))*4 )