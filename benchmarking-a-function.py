import platform, timeit
import numpy as np
import multiprocessing as mp
import matplotlib.pyplot as plt

def print_sysinfo():
	print('\nPython version  :', platform.python_version())
	print('compiler        :', platform.python_compiler())
	print('\nsystem     :', platform.system())
	print('release    :', platform.release())
	print('machine    :', platform.machine())
	print('processor  :', platform.processor())
	print('CPU count  :', mp.cpu_count())
	print('interpreter:', platform.architecture()[0])
	print('\n\n')

def parzen_estimation(x_samples, point_x, h):
	k_n = 0
	for row in x_samples:
		x_i = (point_x - row[:,np.newaxis]) / (h)
		for row in x_i:
			if np.abs(row) > (1/2):
				break
	else: # "completion-else"*
		k_n += 1
	return (h, (k_n / len(x_samples)) / (h**point_x.shape[1]))

def serial(samples, x, widths):
	return [parzen_estimation(samples, x, w) for w in widths]

def multiprocess(processes, samples, x, widths):
	pool = mp.Pool(processes=processes)
	results = [pool.apply_async(parzen_estimation, args=(samples, x, w)) for w in widths]
	results = [p.get() for p in results]
	results.sort() # to sort the results by input window width
	return results

def plot_results(benchmarks):
	bar_labels = range(1, mp.cpu_count()+1)

	fig = plt.figure(figsize=(10,8))

	# plot bars
	y_pos = np.arange(len(benchmarks))
	plt.yticks(y_pos, bar_labels, fontsize=16)
	bars = plt.barh(y_pos, benchmarks,
	align='center', alpha=0.4, color='g')

	# annotation and labels

	for ba,be in zip(bars, benchmarks):
		plt.text(ba.get_width() + 2, ba.get_y() + ba.get_height()/2,
				'{0:.2%}'.format(benchmarks[0]/be),
				ha='center', va='bottom', fontsize=12)

	plt.xlabel('time in seconds for n=%s' %n, fontsize=14)
	plt.ylabel('number of processes', fontsize=14)
	t = plt.title('Serial vs. Multiprocessing via Parzen-window estimation', fontsize=18)
	plt.ylim([-1,len(benchmarks)+0.5])
	plt.xlim([0,max(benchmarks)*1.1])
	plt.vlines(benchmarks[0], -1, len(benchmarks)+0.5, linestyles='dashed')
	plt.grid()

	plt.show()

if __name__ == "__main__" :

	print_sysinfo()
	widths = np.linspace(1.0, 1.2, 100)
	pt = np.array([[0],[0]])

	mu_vec = np.array([0,0])
	cov_mat = np.array([[1,0],[0,1]])
	n = 10000

	x_2Dgauss = np.random.multivariate_normal(mu_vec, cov_mat, n)

	benchmarks = []
	for i in range(1, mp.cpu_count()+1) :
		argsList = ['x_2Dgauss','pt','widths']
		func = 'serial' if i == 1 else 'multiprocess'
		callStr = func+'('+', '.join(([str(i)]+argsList) if i>1 else argsList)+')'
		importStr = 'from __main__ import '+', '.join([func]+argsList)
		# print(callStr, importStr)
		bench = timeit.Timer( callStr, importStr ).timeit(number=1)
		print(bench,"\n")
		benchmarks.append(bench)
		# benchmarks.append([ callStr, importStr ])

	# for call, prereqs in benchmarks :
	# 	print(call,prereqs)
	plot_results(benchmarks)