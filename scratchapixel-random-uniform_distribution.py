import math, random
import matplotlib.pyplot as plt
from matplotlib import rcParams
from functions import *
rcParams.update({'figure.autolayout': True})

# random.random() has uniform distribution
def main() :
	figure = plt.figure()
	random.seed(13)
	numSims = 1000000
	numBins = 100
	bound   = 5
	bins    = [0 for x in range(numBins)] # initialize all bins to zero
	dist    = 10 # km

	for i in range(numSims) :
		diff = (2 * random.random() - 1) * bound
		whichBin = int(numBins * (diff / bound + 1) * 0.5)
		bins[whichBin] += 1
		time = 30 + diff
		speed = 60 * dist / time

	total = 0
	plt.xlabel("")
	plt.ylabel("")
	xAxis = []
	yAxis = []
	for i in range(numBins) :
		r = bins[i] / float(numSims)
		xAxis.append(5 * (2 * (i / float(numBins)) - 1))
		yAxis.append(r)
		total += r
	bufferGraph(0.15, xAxis=xAxis)
	plt.scatter(xAxis,yAxis,s=15)
	plt.plot(
			  xAxis
			, yAxis
			, linewidth=1
			, zorder=3
		)
	plt.grid(zorder=0, color='gray', linestyle='dashed', linewidth=0.5)
	plt.axvline(0, zorder=1, color='k', linewidth=1)
	plt.show()
	plt.close()
	print(total)


main()