import math, random
import matplotlib.pyplot as plt
from matplotlib import rcParams
from functions import *
rcParams.update({'figure.autolayout': True})

def sample(cdf, nbins, minBound, maxBound) :
	r = random.random()
	# offset in the list is the lower bound index, lowerBoundIdx
	lowerBoundIdx = max(0, std__lower_bound(cdf, r) - 1) # can't be less than zero
	upperBoundIdx = lowerBoundIdx + 1
	lowerBoundVal = cdf[lowerBoundIdx]
	upperBoundVal = cdf[upperBoundIdx]
	t = (r - lowerBoundVal) / float(upperBoundVal - lowerBoundVal)
	# map = lowerBound + distance * percentage
	x = minBound + (maxBound - minBound) * ((lowerBoundIdx + t) / float(nbins))
	return x


# standard normal distribution function
def pdf(x) :
	PI = 3.14159265359
	return (1 / math.sqrt(2 * PI) * math.exp(-x * x * 0.5))

def main() :
	figure = plt.figure()
	random.seed(13)

	# create a CDF
	nbins    =  32
	minBound = -5
	maxBound =  5

	cdf      =  [0.0] # first sample should always be zero
	dx       =  (maxBound - minBound) / nbins
	total    =  0
	for n in range(1,nbins) :
		x      = minBound + (maxBound - minBound) * (n / float(nbins))
		pdf_x  = pdf(x) * dx
		cdf.append( cdf[(n-1)] + pdf_x )
		total += pdf_x
	cdf.append(1) # last sample should always be one

	# simulate
	numSims  = 1000000
	numBins  = 100    # for visuals
	bins     = [ 0.0 for x in range(numBins) ]
	distance = 10

	for i in range(numSims) :
		diff = sample(cdf, nbins, minBound, maxBound)
		whichBin = int(numBins * (diff - minBound) / (maxBound - minBound))
		bins[whichBin] += 1
		time  = 30 + diff
		speed = 60 * distance / time

	total = 0
	plt.xlabel("")
	plt.ylabel("")
	xAxis = []
	yAxis = []
	for i in range(numBins) :
		r = bins[i] / float(numSims)
		xAxis.append(5 * (2 * (i / float(numBins)) - 1))
		yAxis.append(r)
		total += r
	bufferGraph(0.05, xAxis=xAxis, yAxis=yAxis)
	plt.scatter(xAxis,yAxis,s=15)
	plt.plot(
			  xAxis
			, yAxis
			, linewidth=1
			, zorder=3
		)
	plt.grid(zorder=0, color='gray', linestyle='dashed', linewidth=0.5)
	plt.axvline(0, zorder=1, color='k', linewidth=1)
	plt.show()
	plt.close()

main()