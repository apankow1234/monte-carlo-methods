import time, multiprocessing

square_results = []

def calc_square(numbers, result, v) :
	for idx, n in enumerate(numbers) :
		v.value += 1
		result[idx] = n*n
		time.sleep(0.2) # for human-readable compare at end

if __name__ == "__main__" :
	test_arr = range(8)
	result = multiprocessing.Array('i', len(test_arr)) # i for integer
	v = multiprocessing.Value('d', 0.0) # d for double
	t = time.time()

	t1 = multiprocessing.Process(target=calc_square, args=(test_arr,result,v))
	t1.start()
	t1.join() # causes wait until finished

	print("done in: ", time.time()-t) # the human readable compare
	print(v.value)
	print(result[:])
