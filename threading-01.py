import time, threading

def calc_square(numbers) :
	results = []
	for n in numbers :
		results.append(n*n)
		time.sleep(0.2) # for human-readable compare at end
	return results

def calc_cube(numbers) :
	results = []
	for n in numbers :
		results.append(n*n*n)
		time.sleep(0.2) # for human-readable compare at end
	return results

test_arr = range(8)
for ToThread in [False,True] :
	print("\n" + ("Multithreaded" if ToThread else "Single Threaded"))
	t = time.time()
	if( not ToThread ) :
		t1 = calc_square(test_arr)
		t2 = calc_cube(test_arr)

		print(t1)
		print(t2)
	else :
		t1 = threading.Thread(target=calc_square, args=(test_arr,))
		t2 = threading.Thread(target=calc_cube, args=(test_arr,))

		t1.start()
		t2.start()

		t1.join() # causes wait until finished
		t2.join() # causes wait until finished

		print(t1)
		print(t2)

	print("done in: ", time.time()-t) # the human readable compare
