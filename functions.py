import os, psutil, threading, multiprocessing
import matplotlib.pyplot as plt

def get_cpu_affinity() :
	if hasattr( os, 'sched_getaffinity' ) :
		return os.sched_getaffinity(0)
	elif psutil is not None :
		proc = psutil.Process()
		if hasattr( proc, 'cpu_affinity' ) :
			return proc.cpu_affinity()
		else :
			return None
	else:
		return None

def get_core_count(usableOnly=True) :
	if usableOnly :
		return len(get_cpu_affinity())
	else :
		return multiprocessing.cpu_count()


def bufferGraph(padding, xAxis=[], yAxis=[]) :
	if xAxis :
		xMinMax   = [min(xAxis), max(xAxis)]
		xPadd     = float(padding) * float(xMinMax[1] - xMinMax[0])
		plt.xlim(xMinMax[0] - xPadd, xMinMax[1] + xPadd)
	if yAxis :
		yMinMax   = [min(yAxis), max(yAxis)]
		yPadd     = float(padding) * float(yMinMax[1] - yMinMax[0])
		plt.ylim(yMinMax[0] - yPadd, yMinMax[1] + yPadd)

def std__lower_bound(searchList, value, sort=True) :
	# based on c/c++ std::lower_bound() for which the function returns
	# the pointer (index, in our case) to a value in the searchList of 
	# equal or greater value.
	# 
	# eg. searchList = [ 1, 3, 3, 5, 7 ]
	# value = 3; returns index to first 3
	# value = 4.3; returns index to 5 because 5 was the next highest value in the searchList
	
	cycleList = searchList[:]
	if sort :
		cycleList.sort()
	for item in cycleList :
		if item >= value :
			return searchList.index(item)
	return None