import multiprocessing

def calc_square(numbers, q) :
	for n in numbers :
		q.put(n*n)

if __name__ == "__main__" :
	test_arr = range(8)
	q = multiprocessing.Queue()

	t1 = multiprocessing.Process(target=calc_square, args=(test_arr,q))
	t1.start()
	t1.join() # causes wait until finish

	while not q.empty() :
		print(q.get())
