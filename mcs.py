import random, os
import matplotlib.pyplot as plt
from matplotlib import rcParams


# 49% chance of winning/ house has advantage by 1%
def rollDice() :
	roll = random.randint(1,100)
	if roll == 100 :
		# print roll, 'roll was 100, you lose. What are the odds?! Play Again!'
		return False
	elif roll <= 50 :
		# print roll, 'roll was 1-50, you lose. Play Again!'
		return False
	elif 100 > roll > 50 :
		# print roll, 'roll was 51-99, you win!. Play More!'
		return True

def simple_bettor(funds, init_wager, wager_count, strategy=None, plotColor=None) :
	global numBrokeBettors
	value=funds
	bet = thisWin = thisLoss = init_wager
	# wager on X-axis
	wX = []
	# value on Y-axis
	vY = []
	wagersMade = 0
	wonLastGame = True # Start on a 'win'
	previousWager = init_wager
	gameOver = False
	while wagersMade < wager_count :
		if strategy == "martingale" :
			bet = previousWager * ( 1 if wonLastGame else 2 )
			thisWin = init_wager
			thisLoss = bet # On loss, double down to make up for losses

		if not gameOver or strategy is None :
			if value - bet < 0 : # don't allow unaffordable debt
				bet = value
			if rollDice() :
				value += bet
				wonLastGame = True
				previousWager = thisWin
			else :
				value -= bet
				wonLastGame = False
				previousWager = thisLoss
			wX.append(wagersMade)
			vY.append(value)

		wagersMade += 1
		if value <= 0 :
			numBrokeBettors += 1
			gameOver = True

		if strategy is not None and gameOver :
			break

	plt.plot(wX, vY, linewidth=0.5, c=plotColor)
	return value


strategies = [None, "martingale"]
for strategy in strategies:
	stratNum = strategies.index(strategy)
	fig = ("0" if stratNum < 10 else "") + str(stratNum)
	for numBettors in [1,100,1000] :
		for wager_count in [100,1000,10000,100000] :
			filename = "fig-"+fig+"-"+str(numBettors)+"b-"+str(wager_count)+"w.png"
			figure = plt.figure()
			plt.xlim(0, wager_count)

			x=0
			numBrokeBettors = 0
			while x < numBettors :
				simple_bettor(10000,100,wager_count, strategy)
				x += 1

			print( filename )

			# deathRate = float( numBrokeBettors / float(x) ) * 100
			# print( 'death rate: ', deathRate )
			# print( 'survival rate: ', 100.0 - float(deathRate) )
			
			plt.axhline(0, color='r')
			plt.ylabel('Account Value')
			plt.xlabel('Wager Count')
			path = "screenshots"+os.sep
			rcParams.update({'figure.autolayout': True})
			figure.savefig(
				path+filename,
				dpi=300,
				orientation='landscape',
				format='png',
				pad_inches=1
			)
			plt.close()

fig = "02"
for numBettors in [1,100,1000] :
	for wager_count in [100,1000,10000,100000] :
		filename = "fig-"+fig+"-"+str(numBettors)+"b-"+str(wager_count)+"w.png"
		figure = plt.figure()
		plt.xlim(0, wager_count)

		x=0
		numBrokeBettors = 0
		while x < numBettors :
			simple_bettor(10000,100,wager_count, plotColor='k')
			simple_bettor(10000,100,wager_count, "martingale", plotColor='c')
			x += 1

		print( filename )		
		plt.axhline(0, color='r')
		plt.ylabel('Account Value')
		plt.xlabel('Wager Count')
		path = "screenshots"+os.sep
		rcParams.update({'figure.autolayout': True})
		figure.savefig(
			path+filename,
			dpi=300,
			orientation='landscape',
			format='png',
			pad_inches=1
		)
		plt.close()
