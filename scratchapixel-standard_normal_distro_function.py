import math
import matplotlib.pyplot as plt
from matplotlib import rcParams
from functions import *
rcParams.update({'figure.autolayout': True})

# standard normal distribution function
def pdf(x) :
	PI = 3.14159265359
	return (1 / math.sqrt(2 * PI) * math.exp(-x * x * 0.5))

def main() :
	figure = plt.figure()
	nbins    =  32
	minBound = -5
	maxBound =  5
	cdf      =  [0.0] # first sample should always be zero
	dx       =  (maxBound - minBound) / nbins
	total    =  0
	for n in range(1,nbins) :
		x      = minBound + (maxBound - minBound) * (n / float(nbins))
		pdf_x  = pdf(x) * dx
		cdf.append( cdf[(n-1)] + pdf_x )
		total += pdf_x

	nsamples = len(cdf)
	cdf.append(1) # last sample should always be 1
	plt.xlabel("Random Variable")
	plt.ylabel("CDF")
	xAxis = []
	yAxis = []
	for n in range(nsamples) :
		xAxis.append(minBound + (maxBound - minBound) * (n / float(nbins)))
		yAxis.append(cdf[n])
	bufferGraph(0.25, xAxis=xAxis, yAxis=yAxis)
	plt.scatter(xAxis,yAxis,s=15)
	plt.plot(
			  xAxis
			, yAxis
			, linewidth=1
			, zorder=3
		)
	plt.grid(zorder=0, color='gray', linestyle='dashed', linewidth=0.5)
	plt.axvline(0, zorder=1, color='k', linewidth=1)
	plt.show()
	plt.close()

main()